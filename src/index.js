import React from 'react';
import ReactDOM from 'react-dom';
import './stylesheets/index.css';
//import App from './App';
//import Card from './components/Card';
import Playmat from './components/Playmat';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Playmat />, document.getElementById('root'));
registerServiceWorker();
