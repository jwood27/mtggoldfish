import React, { Component } from 'react';
//import Card from './components/Card';

const mtg = require('mtgsdk')

class Playmat extends Component {
    constructor(){
        super();
        this.state = {
          setName: '',
          cards: {}
        }
    }
    
    componentDidMount() {
            // fetch the project name, once it retrieves resolve the promise and update the state. 
            //this.getCards('AER');
            console.log(this.cards);
            
            const cardlist = [];

            mtg.card.all({ set: 'AER', pageSize: 1 })
            .on('data', card => {
                //console.log(card.name)
                cardlist.push(card)
            })
            
            this.setState({
                cards: cardlist
            })

        }
        
    getCards(index){
        
        mtg.card.all({set: index})
        .on('data', card => this.setState({
            setName: card[0].name,
            cards: card // "Aether Revolt"
        }))
        
    }
  
    render(){
        const setName = 'dsaf';
        return(
            <div>
              <h2>{setName}</h2>
              {this.cards && this.cards.length > 0 ? (
                <ul>
                  {this.cards.map((cards, index) => {
                    return (
                      <li key={index}>
                        {cards.name}
                      </li>
                    );
                  })}
                </ul>
              ) : null}
            
            </div>
            );
    }
}

export default Playmat;