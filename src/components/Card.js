import React, { Component } from 'react';
import '../stylesheets/Card.css';

const mtg = require('mtgsdk')

class Card extends Component {
  constructor(){
    super();
    this.state = {
      cardname: '',
      cardimage: ''
    }
  }

  componentDidMount() {
    // fetch the project name, once it retrieves resolve the promise and update the state. 
    this.getCard(3);
  }

  getCard(index){
    mtg.card.find(index).then(result => this.setState({
      cardname: result.card.name,
      cardimage: result.card.imageUrl
    }))
  }

  render(){
    const cardname = this.state.cardname;
    const cardimage = this.state.cardimage;

    return (
      <div className='card'>
        <img src={cardimage} />  
        <span className='img-label'>{cardname}</span>
        <button onClick={() => this.getCard(Math.floor((Math.random() * 5000) + 1))}>Get Random Card</button>
      </div>
    );
  }
}



export default Card;